GameState = {}
GameState.__index = GameState

local state = {
    Start = 0,
    Playing = 1,
    Serving = 2,
    Finished = 3,
}

function GameState:default()
    local gameState = {}
    setmetatable(gameState, GameState)

    gameState.score = Score:default()
    gameState.serving = PLAYER_1
    gameState.state = state.Start

    return gameState
end

function GameState:nextState()
    if self:isStart() or self:isFinished() then
        self.state = state.Serving
        self.score = Score:default()
    elseif self:isPlaying() then
        if self.score.player1 >= WINNING_SCORE or self.score.player2 >= WINNING_SCORE then
            self.state = state.Finished
        else
            self.state = state.Serving
        end
    elseif self:isServing() then
        self.state = state.Playing
    end
end

function GameState:addScore(player)
    if player == PLAYER_1 then
        self.score.player1 = self.score.player1 + 1
    else
        self.score.player2 = self.score.player2 + 1
    end

    self.serving = self.serving == PLAYER_1 and PLAYER_2 or PLAYER_1
end

function GameState:isStart()
    return self.state == state.Start
end

function GameState:isPlaying()
    return self.state == state.Playing
end

function GameState:isServing()
    return self.state == state.Serving
end

function GameState:isFinished()
    return self.state == state.Finished
end

function GameState:winner()
    return self.score.player1 == WINNING_SCORE and PLAYER_1 or PLAYER_2
end

function GameState:restart()
    self.score = Score:default()
    self.serving = PLAYER_1
    self.state = state.Start
end
