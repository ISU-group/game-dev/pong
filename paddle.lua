Paddle = {}
Paddle.__index = Paddle

function Paddle:new(x, y, width, height)
    local paddle = {}
    setmetatable(paddle, Paddle)

    paddle.location = Vector:new(x, y)
    paddle.width = width
    paddle.height = height
    paddle.speedY = 0

    return paddle
end

function Paddle:update(dt)
    local newY = self.location.y + self.speedY * dt

    if self.speedY < 0 then
        self.location.y = math.max(0, newY)
    else
        self.location.y = math.min(love.graphics.getHeight() - self.height, newY)
    end
end

function Paddle:draw()
    love.graphics.rectangle("fill", self.location.x, self.location.y, self.width, self.height)
end
