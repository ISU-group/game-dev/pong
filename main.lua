require "ball"
require "paddle"
require "score"
require "constants"
require "game_state"
require "vector"
require "resources"
require "bot"
require "user"

local resources
local gameState
local player1
local player2
local ball

local function loadDefaultResources()
    local fonts = {
        ["small"] = love.graphics.newFont("resources/fonts/AtariClassic-Regular.ttf", 12),
        ["medium"] = love.graphics.newFont("resources/fonts/AtariClassic-Regular.ttf", 16),
        ["large"] = love.graphics.newFont("resources/fonts/AtariClassic-Regular.ttf", 32),
    }

    local sounds = {
        ["pong"] = love.audio.newSource("resources/sounds/pong.wav", "static"),
        ["gameOver"] = love.audio.newSource("resources/sounds/gameover.wav", "static"),
        ["victory"] = love.audio.newSource("resources/sounds/victory.wav", "static"),
    }

    return Resources:new(fonts, sounds)
end

function love.load()
    resources = loadDefaultResources()
    gameState = GameState:default()

    local paddle1 = Paddle:new(
        PADDLE_OFFSET_X,
        PADDLE_OFFSET_Y,
        PADDLE_WIDTH,
        PADDLE_HEIGHT
    )
    local paddle2 = Paddle:new(
        SCREEN_WIDTH - PADDLE_OFFSET_X,
        SCREEN_HEIGHT - PADDLE_OFFSET_Y - PADDLE_HEIGHT,
        PADDLE_WIDTH,
        PADDLE_HEIGHT
    )

    player1 = User:new(paddle1)
    player2 = Bot:new(paddle2)

    ball = Ball:new(
        (SCREEN_WIDTH - BALL_SIZE) / 2,
        (SCREEN_HEIGHT - BALL_SIZE) / 2,
        BALL_SIZE
    )
end

local function ballInServe()
    ball:setRandomVelocity()
    if gameState.serving == PLAYER_2 then
        ball.velocity.x = -ball.velocity.x
    end
end

local function ballPaddleCollision()
    ball.velocity.x = -math.min(MAX_BALL_SPEED_X, ball.velocity.x * BALL_SPEED_ACCELERATION_X)
    ball.velocity.y = ball.velocity.y > 0 and math.random(MIN_BALL_SPEED_Y, MAX_BALL_SPEED_Y)
        or -math.random(MIN_BALL_SPEED_Y, MAX_BALL_SPEED_Y)
end

local function updateBallBoundaries()
    if ball.location.y <= 0 then
        ball.location.y = 0
        ball.velocity.y = -ball.velocity.y
    elseif ball.location.y + ball.size >= SCREEN_HEIGHT then
        ball.location.y = SCREEN_HEIGHT - ball.size
        ball.velocity.y = -ball.velocity.y
    end

    if ball.location.x <= 0 then
        gameState:addScore(PLAYER_2)
        gameState:nextState()
        ball:reset()

        if gameState:isFinished() then
            resources.sounds.gameOver:play()
        end
    elseif ball.location.x + ball.size >= SCREEN_WIDTH then
        gameState:addScore(PLAYER_1)
        gameState:nextState()
        ball:reset()

        if gameState:isFinished() then
            resources.sounds.victory:play()
        end
    end
end

function love.update(dt)
    if gameState:isServing() then
        ballInServe()
        gameState:nextState()
    elseif gameState:isPlaying() then
        if ball:hasCollision(player1.paddle) then
            ball.location.x = player1.paddle.location.x + player1.paddle.width
            ballPaddleCollision()
            resources.sounds.pong:play()
        elseif ball:hasCollision(player2.paddle) then
            ball.location.x = player2.paddle.location.x - ball.size
            ballPaddleCollision()
            resources.sounds.pong:play()
        end

        updateBallBoundaries()

        ball:update(dt)
    end

    player1:update(dt)
    player2:update(dt, ball)
end

local function drawBackground()
    local halfWidth = SCREEN_WIDTH / 2
    local separateLineWidth = 5

    local r, g, b, a = love.graphics.getColor()

    love.graphics.setColor(love.math.colorFromBytes(32, 16, 32))
    love.graphics.rectangle("fill", halfWidth - separateLineWidth / 2, 0, separateLineWidth, SCREEN_HEIGHT)
    love.graphics.setColor(r, g, b, a)

    love.graphics.setFont(resources.fonts.large)
    love.graphics.printf(tostring(gameState.score.player1), 0, 20, halfWidth, "center")
    love.graphics.printf(tostring(gameState.score.player2), halfWidth, 20, halfWidth, "center")
end

function love.draw()
    drawBackground()

    if gameState:isStart() then
        love.graphics.setFont(resources.fonts.medium)
        love.graphics.printf("This is Pong game!", 0, 40, SCREEN_WIDTH, "center")
        love.graphics.setFont(resources.fonts.small)
        love.graphics.printf("Press Space to start", 0, 80, SCREEN_WIDTH, "center")
        love.graphics.printf("or R to restart", 0, 100, SCREEN_WIDTH, "center")
    elseif gameState:isServing() then
        love.graphics.setFont(resources.fonts.medium)
        love.graphics.printf("Player " .. tostring(gameState.serving) .. "\'s turn", 0, 40, SCREEN_WIDTH, "center")
    elseif gameState:isFinished() then
        local message = ""
        if gameState:winner() == PLAYER_1 then
            message = "Victory!"
        else
            message = "Game over :("
        end
        love.graphics.setFont(resources.fonts.large)
        love.graphics.printf(message, 0, 80, SCREEN_WIDTH, "center")

        love.graphics.setFont(resources.fonts.small)
        love.graphics.printf("Press Space to start", 0, 140, SCREEN_WIDTH, "center")
    end


    player1.paddle:draw()
    player2.paddle:draw()
    ball:draw()
end

function love.keypressed(key, scancode, isrepeat)
    if key == "space" and not gameState:isPlaying() then
        gameState:nextState()
    end
    if key == "r" then
        gameState:restart()
        ball:reset()
    end
end
