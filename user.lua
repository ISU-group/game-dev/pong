User = {}
User.__index = User

function User:new(paddle)
    local user = {}
    setmetatable(user, User)

    user.paddle = paddle

    return user
end

function User:update(dt)
    local isUp = love.keyboard.isScancodeDown('w', 'up')
    local isDown = love.keyboard.isScancodeDown('s', 'down')

    if isUp then
        self.paddle.speedY = -PADDLE_SPEED
    elseif isDown then
        self.paddle.speedY = PADDLE_SPEED
    else
        self.paddle.speedY = 0
    end

    self.paddle:update(dt)
end
