Resources = {}
Resources.__index = Resources

function Resources:new(fonts, sounds)
    local resources = {}
    setmetatable(resources, Resources)

    resources.fonts = fonts
    resources.sounds = sounds

    return resources
end
