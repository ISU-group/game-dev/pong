Bot = {}
Bot.__index = Bot

function Bot:new(paddle)
    local bot = {}
    setmetatable(bot, Bot)

    bot.paddle = paddle
    return bot
end

function Bot:update(dt, ball)
    local paddleBottomY = self.paddle.location.y + self.paddle.height
    local ballCenterY = ball.location.y + ball.size / 2

    if ballCenterY <= paddleBottomY and ballCenterY >= self.paddle.location.y then
        self.paddle.speedY = 0
    elseif ballCenterY > paddleBottomY then
        self.paddle.speedY = PADDLE_SPEED
    else
        self.paddle.speedY = -PADDLE_SPEED
    end

    self.paddle:update(dt)
end
