Score = {}
Score.__index = Score

function Score:default()
    local score = {}
    setmetatable(score, Score)

    score.player1 = 0
    score.player2 = 0

    return score
end
