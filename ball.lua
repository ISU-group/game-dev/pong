Ball = {}
Ball.__index = Ball

function Ball:new(x, y, size)
    local ball = {}
    setmetatable(ball, Ball)

    ball.location = Vector:new(x, y)
    ball.size = size
    ball.velocity = Vector:new(0, 0)

    return ball
end

function Ball:hasCollision(paddle)
    if self.location.x > paddle.location.x + paddle.width or
        paddle.location.x > self.location.x + self.size or
        self.location.y > paddle.location.y + paddle.height or
        paddle.location.y > self.location.y + self.size
    then
        return false
    end

    return true
end

function Ball:reset()
    self.location = Vector:new(
        (SCREEN_WIDTH - BALL_SIZE) / 2,
        (SCREEN_HEIGHT - BALL_SIZE) / 2
    )
    self.velocity = Vector:new(0, 0)
end

function Ball:update(dt)
    self.location = self.location + self.velocity * dt
end

function Ball:draw()
    love.graphics.rectangle("fill", self.location.x, self.location.y, self.size, self.size)
end

function Ball:setRandomVelocity()
    self.velocity = Vector:new(
        math.random(MIN_BALL_SPEED_X, MAX_BALL_SPEED_X),
        math.random(-MAX_BALL_SPEED_Y, MAX_BALL_SPEED_Y)
    )
end
